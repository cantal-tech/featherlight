<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function mediabox_config_theme() {

	//$couleur = parametre_url(parametres_css_prive(),'couleur_claire');

	$config = array(
		'namespace' => 'modal',
		'couleur' => '#FBFBFB',
		'opacite' => '0.9',
		'maxWidth' => '960px'
	);
	return $config;
}
