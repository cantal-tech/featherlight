# FEATHERLIGHT pour SPIP

Mise en paquet de [featherlight](https://github.com/noelboss/featherlight) pour SPIP via l'API Mediabox.
Nécessite l'API Mediabox v2 pour SPIP disponible [ici](https://framagit.org/cantal-tech/mediabox).

## Caractéristiques de la modalbox :
- Simple, légère
- Option galerie optionnelle (fichier séparé)
- **Pas de resize** ni d'animation en javascript (uniquement en CSS : media-queries + transition)
- Le plugin fournit un modèle dynamique (featherlight.css.html) qui produit un fichier CSS en fonction des préférences saisies dans le formulaire configurer_mediabox. Nouveauté : on peut choisir une couleur de fond pour l'overlay.
- Support swipe optionnel (dispositif tactile). 

## NB : 
- Le plugin featherlight ne supporte pas l'option **autoResize** de l'API mediabox.