<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Tableau de la configuration du plugin
 * @return array       
**/
function featherlight_config() {

	$config = array(
		'nom' => 'featherlight',
		'options' => array(
			'opacite' => '0.9',
			'couleur' => '#222222'
		),
		'fichiers' => array(
			'core' => array(
				'fichier' => 'featherlight.min.js',
				'chemin' => 'lib/featherlight/'
			),
			'galerie' => array(
				'fichier' => 'featherlight.gallery.min.js',
				'chemin' => 'lib/featherlight/'
			),
			'mediabox' => array(
				'fichier' => 'featherlight.mediabox.js', 
				'chemin' => 'javascript/'
			),					
		),
	);
	return $config;
}

/**
 * Pipeline mediabox_config
 * @return array       
**/
function featherlight_mediabox_config($flux) {
	
	$conf = featherlight_config();
	// completer les options manquantes
	foreach ($conf['options'] as $key => $value) {
		if (!isset($flux[$key]))	 {
			$flux[$key] = $value;
		}
	}
	//ajouter featherlight à la liste des libs disponibles
	$flux['_libs']['featherlight'] = $conf['fichiers'];
	return $flux;
}


/**
 * Pipeline insert_head_css
**/
// function featherlight_insert_head_css($flux) {
//
// 	$config = mediabox_config();
// 	// cas particulier du fichier css dissocié : featherlight.gallery.css
// 	if (
// 		$config['active'] == 'oui'
// 		AND $config['skin'] != 'none'
// 		AND $config['lib'] == 'featherlight'
// 		AND !empty($config['selecteur_galerie'])
// 		) {
// 		$motif = (test_espace_prive() ? 'prive/' : '') . $config['lib']. '/' . $config['skin'] . '/' . $config['lib'];
// 		$flux .= trouver_ou_produire($motif.'.gallery.css','',true);
// 	}
//
// 	return $flux;
// }
